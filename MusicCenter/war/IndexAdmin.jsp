<%@page import="java.util.ArrayList, pweb2.Usuario1,pweb2.Producto,pweb2.Pedido,pweb2.Venta,pweb2.Asesoria, pweb2.BD"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>MusicCenter</title>
	<link rel="stylesheet" href="estiloAdmin.css">
	<link rel="stylesheet" href="estiloSlider.css">
	<link rel="stylesheet" href="fonts.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap core CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet">              
	<!-- Custom styles for this template -->
	<link href="assets/css/style.css" rel="stylesheet">   
	<script src="assets/js/chart-master/Chart.js"></script>
</head>

<body>
<% ArrayList <Producto> product= BD.getListaprod();
	int tamanoprod = product.size();
	int tamanoped = BD.getListapedi().size();
	int tamanousuario = BD.getMylist().size();
	int tamanoventas = BD.getListaventa().size();
	int tamanoasesoria = BD.getListaasesoria().size();
	
						
%>

<div id="cabecera">
	<div id="menusup">
    	<div class="contenedormenusup">
        	<ul id="menu-bar">
                 <li class="active"><a href="IndexAdmin.jsp">INICIO</a></li>
                 <li><a href="RegistrarProd.html">PRODUCTOS</a></li>			                
                 <li><a href="#">INFORMES</a></li>
                 <li><a href="formVentas.html">REGISTRO VENTAS</a></li>
                 <li><a href="index.html">CERRAR SESI�N</a></li>
        	</ul>
            
           
            <div class="login">
            
   
          </div>
    
            <div class="buscar">
            
            </div>
        </div>
    </div>
    <div id="menuinf">
    	<div id="menuinfcentral" class="container">
            <div id="logo">
                <a href="index.html"><img src="images/logo.png" alt=""/></a>
            </div>
            <div id="menugeneral">
            <script src="ddmenu.js" type="text/javascript"></script>
           		<nav id="ddmenu">
                <ul>
                    <li class="full-width">
                        <span class="top-heading">BAJOS Y GUITARRAS</span>
                        <i class="caret"></i>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Guitarras</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="GuitarraElec.html">Guitarras El�ctricas</a></li>
                                    <li><a href="#">Guitarras Ac�sticas</a></li>
                                   
                                    <li><a href="#">Destacados</a></li>
                                    
                                </ul>
                                <ul class="column">
                                    <li><h3>Bajos</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Bajos El�ctricos</a></li>
                                    <li><a href="#">Bajos Ac�sticos</a></li>
                                    <li><a href="#">Baby Bass</a></li>
                                    
                                </ul>
                                <ul class="column mayHide">
                                    <li><br /><img src="images/menuguitar.png" /></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a class="top-heading" href="http://www.microsoft.com">VIENTOS</a>
                        <i class="caret"></i>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Yamaha</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Vientos Madera</a></li>
                                    <li><a href="#">Vientos Metal</a></li>  
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <span class="top-heading">TECLADOS</span>
                        <i class="caret"></i>
                        <div class="dropdown offset300">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Pianos</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Pianos de Cola</a></li>
                                    <li><a href="#">Pianos de Pared</a></li>
                                   
                                    <li><h3>Clavinovas</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Destacados</a></li>
                                   
                                </ul>
                                <ul class="column">
                                    <li><h3>Teclados y Sintetizadores</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Teclados El�ctricos</a></li>
                                    <li><a href="#">Sintetizadores</a></li>
                                    <li><a href="#">Accesorios</a></li>
                              
                                </ul>
                                <ul class="column mayHide">
                                    <li><br /><img src="images/menupiano.png" /></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <span class="top-heading">PERCUSION</span>
                        <i class="caret"></i>
                        <div class="dropdown right-aligned">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Bater�as</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Bater�a Ac�stica</a></li>
                                    <li><a href="#">Bater�a Digital</a></li>
                                 
                                </ul>
                                <ul class="column">
                                    <li><h3>Percusi�n</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Percusi�n Latina</a></li>
                                    <li><a href="#">Baquetas </a></li>
                                    <li><a href="#">Platillos</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li class="no-sub">
                        <a class="top-heading" href="#">AMPLIFICADORES</a>
                    </li>
                    
                </ul>
            </nav>
                
        	</div>
         </div>
    </div>
</div>
<div id="cuerpo">
	<div class= "slides">
    		<img src="img/imagen1.jpg" alt="">
			<img src="img/imagen2.jpg" alt="">
			<img src="images/BannerPiano.jpg" alt="">
			<img src="img/imagen4.jpg" alt="">
			<img src="images/bannerfen.jpg" alt="">
    </div>
 
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="js/jquery.slides.js"></script>
	<script>
		$(function(){
		  $(".slides").slidesjs({
			play: {
			  active: false,
				// [boolean] Generate the play and stop buttons.
				// You cannot use your own buttons. Sorry.
			  effect: "slide",
				// [string] Can be either "slide" or "fade".
			  interval: 3000,
				// [number] Time spent on each slide in milliseconds.
			  auto: true,
				// [boolean] Start playing the slideshow on load.
			  swap: true,
				// [boolean] show/hide stop and play buttons
			  pauseOnHover: false,
				// [boolean] pause a playing slideshow on hover
			  restartDelay: 2500
				// [number] restart delay on inactive slideshow
			}
		  });
		});
	</script>
    <div id="contenidogeneral">
    <div id="titulo"> 
    	<h2 style="font-size: 36px; font-style: inherit; font-weight: bold;"> PANEL DE ADMINISTRADOR </h2>
    

        <a href="http://www.clker.com/cliparts/w/8/I/z/1/c/brown-divider-no-background.svg.med.png"><img style="margin-left:150px; margin-top:10px;" src='http://www.clker.com/cliparts/w/8/I/z/1/c/brown-divider-no-background.svg.med.png' alt='Brown Divider No Background clip art'/></a>
                
    </div>
   <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn donut-chart">
                      			<div class="white-header">
						  			<h5>PORCENTAJE DE INFORMACI�N</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-database"></i> 3%</p>
									</div>
	                      		</div>
								<canvas id="serverstatus01" height="120" width="120"></canvas>
								<script>
									var doughnutData = [
											{
												value: 3,
												color:"#68dff0"
											},
											{
												value : 97,
												color : "#fdfdfd"
											}
										];
										var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);
								</script>
	                      	</div><! --/grey-panel -->
                      	</div><!-- /col-md-4-->
                      	

                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn">
                      			<div class="white-header">
						  			<h5>PRODUCTOS REGISTRADOS</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-music"></i><%out.println(tamanoprod);%></p>
									</div>
									<div class="col-sm-6 col-xs-6"></div>
	                      		</div>
	                      		<div class="centered">
										<img src="images/producto.png" width="120">
	                      		</div>
                      		</div>
                      	</div><!-- /col-md-4 -->
                      	
						<div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<div class="white-panel pn">
								<div class="white-header">
									<h5>Usuarios Registrados</h5>
								</div>
								<p><img src="images/ui-zac.jpg" class="img-circle" width="100"></p>
								<p><b>Total de Usuarios</b></p>
                                <p><b><%out.println(tamanousuario);%></b></p>
								
							</div>
						</div><!-- /col-md-4 -->
                      	

                    </div>
                    <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn">
                      			<div class="white-header">
						  			<h5>VENTAS REGISTRADAS</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-music"></i><%out.println(tamanoventas);%></p>
									</div>
									<div class="col-sm-6 col-xs-6"></div>
	                      		</div>
	                      		<div class="centered" >
										<img src="images/registroventas.jpg" width="180">
	                      		</div>
                      		</div>
	                      	
                      	</div><!-- /col-md-4-->
                      	

                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="grey-panel pn">
								<div class="grey-header">
									<h5>REGISTRO DE PEDIDOS</h5>
								</div>
								<div class="chart mt">
									<div class="sparkline"></div>
								</div>
								<h3 class="mt"><b><%out.println(tamanoped);%></b><br><br>Pedidos de Usuarios</h3>
							</div>
                      	</div><!-- /col-md-4 -->
                      	
						<div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<div class="white-panel pn">
								<div class="white-header">
									<h5>ASESOR�AS</h5>
								</div>
								<p><img src="images/asesores.jpg" class="img-circle" width="100"></p>
								<p><b>Consultas Recibidas</b></p>
                                <p><b><%out.println(tamanoasesoria);%></b></p>
								
							</div>
						</div><!-- /col-md-4 -->
                      	

                    </div>
             </div>
    
</div>
<div id="footer">
	<div class="contenedor">
    	<div class="footer-left">

				<h3>Music<span>Center</span></h3>

				<p class="footer-links">
					<a href="IndexAdmin.jsp">Inicio</a>
					�
					<a href="#">Guitarras y Bajos</a>
					�
					<a href="#">Vientos</a>
					�
					<a href="#">Teclados</a>
					�
					<a href="#">Percusi&oacute;n</a>
					�
					<a href="#">Amplificadores</a>
				</p>
                <hr>
                <p class="footer-company-name">Programaci&oacute;n Web 2 &copy; 2016</p>

				
		</div>
        
        <div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>	
Av. Victor Andr�s Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>201-1800 * 242-6377</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
				</div>

			</div>
        
        <div class="footer-right">

				<p class="footer-company-about">
					<span>Horarios de Atenci&oacute;n</span>
                    Atenci�n de lunes a s�bados de 11:00am. a 8:00pm.	
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

		</div>
    
    </div>
</div>

</body>
</html>
    