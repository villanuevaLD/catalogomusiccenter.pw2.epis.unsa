<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>MusicCenter</title>
<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="estilo2.css">
<link rel="stylesheet" href="estiloFormulario.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">


</head>

<body>
<div id="cabecera">
	<div id="menusup">
    	<div class="contenedormenusup">
        	<ul id="menu-bar">
                 <li><a href="IndexUsuario.jsp">INICIO</a></li>
                 <li><a href="Conocenos.html">CONOCENOS</a></li>
                 <li class="active" ><a href="Asesorias.jsp">ASESOR&Iacute;AS</a></li>
                 <li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
                 
        	</ul>
            
        </div>
    </div>
    <div id="menuinf">
   	  <div id="menuinfcentral" class="container">
        <div id="logo">
                <a href="index.html"><img style="margin-left:20px;" src="images/logo.png" alt=""/></a>
          </div>
          <div id="menugeneral">
            <script src="ddmenu.js" type="text/javascript"></script>
           		<nav id="ddmenu">
                <ul>
                    <li class="full-width">
                        <span class="top-heading">BAJOS Y GUITARRAS</span>
                        <i class="caret"></i>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Guitarras</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Guitarras El�ctricas</a></li>
                                    <li><a href="#">Guitarras Ac�sticas</a></li>
                                   
                                    <li><a href="#">Destacados</a></li>
                                    
                                </ul>
                                <ul class="column">
                                    <li><h3>Bajos</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Bajos El�ctricos</a></li>
                                    <li><a href="#">Bajos Ac�sticos</a></li>
                                    <li><a href="#">Baby Bass</a></li>
                                    
                                </ul>
                                <ul class="column mayHide">
                                    <li><br /><img src="images/menuguitar.png" /></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a class="top-heading" href="">VIENTOS</a>
                        <i class="caret"></i>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Yamaha</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Vientos Madera</a></li>
                                    <li><a href="#">Vientos Metal</a></li>  
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <span class="top-heading">TECLADOS</span>
                        <i class="caret"></i>
                        <div class="dropdown offset300">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Pianos</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Pianos de Cola</a></li>
                                    <li><a href="#">Pianos de Pared</a></li>
                                   
                                    <li><h3>Clavinovas</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Destacados</a></li>
                                   
                                </ul>
                                <ul class="column">
                                    <li><h3>Teclados y Sintetizadores</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Teclados El�ctricos</a></li>
                                    <li><a href="#">Sintetizadores</a></li>
                                    <li><a href="#">Accesorios</a></li>
                              
                                </ul>
                                <ul class="column mayHide">
                                    <li><br /><img src="images/menupiano.png" /></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <span class="top-heading">PERCUSION</span>
                        <i class="caret"></i>
                        <div class="dropdown right-aligned">
                            <div class="dd-inner">
                                <ul class="column">
                                    <li><h3>Bater�as</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Bater�a Ac�stica</a></li>
                                    <li><a href="#">Bater�a Digital</a></li>
                                 
                                </ul>
                                <ul class="column">
                                    <li><h3>Percusi�n</h3></li>
                                    <li><a href="#">Ver Todo</a></li>
                                    <li><a href="#">Percusi�n Latina</a></li>
                                    <li><a href="#">Baquetas </a></li>
                                    <li><a href="#">Platillos</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <li class="no-sub">
                        <a class="top-heading" href="#">AMPLIFICADORES</a>
                    </li>
                    
                </ul>
            </nav>
                
        	</div>
    	</div>
    </div>
</div>
<div id="cuerpo">
    <div id="contenidogeneral">
			<div class="page-header">
            	<img style="margin-top:-30px;" src="images/asesoria.png">
				<h2>ASESOR�A DE PRODUCTOS</h2>
			</div>
    	<div class="contacto">
    	  <!-- INICIO DE FORMULARIO --> 
			<form method='post' action="asesoriaServlet">
				<fieldset id="izq"  style="float:left; width: 45%; border-radius: 4px; border-color:#999;">
						<legend>Por Favor, Responda algunos datos sobre Usted </legend>
				<div class="fila">
                	
					<div class="etiqueta"><label for="nombre">Nombre :</label></div>
					<div class="control"><input type="text" name="nombre" required/></div>
				</div>
				<div class="fila">
					<div class="etiqueta"><label for="apellido">Apellidos :</label></div>
					<div class="control"><input type="text" name="apellido" required/></div>
				</div>
				
				<div class="fila">
					<div class="etiqueta"><label for="correo">Correo :</label></div>
					<div class="control"><input type="email" name="correo" maxlength="30"  title="apellidos" required/></div>
				</div>
                </fieldset>
                
                <fieldset id="der" style="float:right; width: 45%; border-radius:4px; border-color:#999;">
				 <legend>Consulta </legend>
				<div class="fila">
               
                	<div class="fila">
						<div class="etiqueta"><label for="nombreinst">Ingrese Nombre del Instrumento:</label></div>
						<div class="control"><input type="text" name="nombreinst" maxlength="20" 
							 title="nombre" required/></div>
					</div>
                    <div class="fila">
						<div class="etiqueta"><label for="marca">Ingrese la Marca:</label></div>
						<div class="control"><input type="text" name="marca" maxlength="20" 
							 title="marca" required/></div>
					</div>
                     <div class="fila">
						<div class="etiqueta"><label for="categoria">Elija la Categor�a:</label></div>
						<div class="control">
							<select name="categoria" size="1">
								<option value=""></option>
								<option value="GuitarrasBajos">Guitarras y Bajos</option>
								<option value="AmplificadoresSonidos">Amplificadores y Sonidos</option>
								<option value="Teclados">Teclados</option>
								<option value="Percusion">Percusi�n</option>
								<option value="Viento">Vientos</option>
								<option value="Cuerda">Cuerda</option>	 			
							</select>
						</div>
					</div>
                    <div class="fila">
						<div class="etiqueta"><label for="detalle">Detalle la Consulta sobre el Producto:</label></div>
						<div class="control"><textarea name="detalle" rows="10" cols="40"></textarea></div>
					</div>
                    
                <br>
                
                
                </fieldset>
                
				<div class="fila">
					<div id="action" style="margin-top:20px;"><input type="reset" value="Limpiar"/><br><input type="submit" value="Consultar"/></div>
				</div>
                
			</form>
		  
		  <!-- FIN DE FORMULARIO --> 
		  
        </div>
       
    </div>
</div>
<div id="footer">
	<div class="contenedor">
    	<div class="footer-left">

				<h3>Music<span>Center</span></h3>

				<p class="footer-links">
					<a href="index.html">Inicio</a>
					�
					<a href="#">Guitarras y Bajos</a>
					�
					<a href="#">Vientos</a>
					�
					<a href="#">Teclados</a>
					�
					<a href="#">Percusi&oacute;n</a>
					�
					<a href="#">Amplificadores</a>
				</p>
                <hr>
                <p class="footer-company-name">Programaci&oacute;n Web 2 &copy; 2016</p>

				
		</div>
        
        <div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>	
Av. Victor Andr�s Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>201-1800 * 242-6377</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
				</div>

			</div>
        
        <div class="footer-right">

				<p class="footer-company-about">
					<span>Horarios de Atenci&oacute;n</span>
                    Atenci�n de lunes a s�bados de 11:00am. a 8:00pm.	
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

		</div>
    
    </div>
<div>

</body>
</html>
    