package pweb2;


public class Usuario1 {

	private String name;
	
	private String lastname;
	
	private String email;
	
	private String ciudad;
	
	private String direccion;
	
	private String tipoUsuario;
	
	private String idUsuario;
	
	private String password;

	public Usuario1(String name, String lastname, String email,
			String ciudad, String direccion, String tipoUsuario,
			String idUsuario, String password) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.email = email;
		this.ciudad = ciudad;
		this.direccion = direccion;
		this.tipoUsuario = tipoUsuario;
		this.idUsuario = idUsuario;
		this.password = password;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

}
