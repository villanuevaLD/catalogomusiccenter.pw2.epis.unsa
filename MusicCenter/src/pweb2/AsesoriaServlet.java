package pweb2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class AsesoriaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			String name = req.getParameter("nombre");
			String apellido = req.getParameter("apellido");
			String correo = req.getParameter("correo");
			String nombreinst = req.getParameter("nombreinst");
			String marca = req.getParameter("marca");
			String categoria=req.getParameter("categoria");
			String detalle = req.getParameter("detalle");
			
			Asesoria nuevo = new Asesoria(name,apellido,correo,nombreinst,marca,categoria,detalle);
			BD.listaasesoria.add(nuevo);
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Consulta Enviada Correctamente');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("IndexUsuario.jsp");
		    rs.forward(req, resp);
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Enviar Consulta, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Asesorias.jsp");
	        rs.forward(req, resp);
		}
	}
}
