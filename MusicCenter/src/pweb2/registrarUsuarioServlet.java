package pweb2;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class registrarUsuarioServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			
			String name = req.getParameter("nombre");
			String lastname = req.getParameter("apellido");
			String email = req.getParameter("correo");
			String ciudad = req.getParameter("ciudad");
			String direccion = req.getParameter("direccion");
			String tipo ="cliente";
			String id = req.getParameter("nickname");
			String contrasena = req.getParameter("contrasena");
			
			Usuario1 nuevo = new Usuario1(name,lastname,email,ciudad,direccion,tipo,id,contrasena);
			BD.mylist.add(nuevo);
			
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Usuario registrado exitosamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("index.html");
		    rs.forward(req, resp);
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se puedo registrar usuario, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("RegistrarUsuario.jsp");
	        rs.forward(req, resp);
		}
	}
}
