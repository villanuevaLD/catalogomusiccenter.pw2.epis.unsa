package pweb2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class registrarProdServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			
			String name = req.getParameter("nombre");
			String marca = req.getParameter("marca");
			String modelo = req.getParameter("modelo");
			String categoria = req.getParameter("categoria");
			String precioneto = req.getParameter("precioNeto");
			String preciobruto = req.getParameter("precioBruto");
			String stock= req.getParameter("stock");
			String preciodscto = req.getParameter("preciodescuento");
			String longitud=req.getParameter("longitud");
			String ancho=req.getParameter("ancho");
			String altura=req.getParameter("altura");
			String peso=req.getParameter("peso");
			String descripcion=req.getParameter("descripcion");
			
			double precioneto1=Double.parseDouble(precioneto);
			double preciobruto1=Double.parseDouble(preciobruto);
			int stock1=Integer.parseInt(stock);
			double preciodscto1=Double.parseDouble(preciodscto);
			double longitud1=Double.parseDouble(longitud);
			double ancho1=Double.parseDouble(ancho);
			double altura1=Double.parseDouble(altura);
			double peso1=Double.parseDouble(peso);

			
			Producto nuevo = new Producto(name,marca,modelo,categoria,precioneto1,preciobruto1,stock1,preciodscto1,longitud1,ancho1,altura1,peso1,descripcion);
			BD.listaprod.add(nuevo);
			
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Producto Almacenado Correctamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("IndexAdmin.jsp");
	        rs.forward(req, resp);
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Registrar el Producto, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("RegistrarProd.html");
	        rs.forward(req, resp);
		}
	}
}
