package pweb2;

public class Pedido {
	
	private String name;
	private String lastname;
	private String direccion;
	private String ciudad;
	private String email;
	
	private String instrumento;
	private String modelo;
	private String marca;
	private String categoría;
	private String observaciones;
	public Pedido(String name, String lastname, String direccion, String ciudad, String email, String instrumento,
			String modelo, String marca, String categoría, String observaciones) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.email = email;
		this.instrumento = instrumento;
		this.modelo = modelo;
		this.marca = marca;
		this.categoría = categoría;
		this.observaciones = observaciones;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInstrumento() {
		return instrumento;
	}
	public void setInstrumento(String instrumento) {
		this.instrumento = instrumento;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getCategoría() {
		return categoría;
	}
	public void setCategoría(String categoría) {
		this.categoría = categoría;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}
