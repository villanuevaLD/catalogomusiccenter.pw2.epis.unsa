package pweb2;

public class Asesoria {
	private String name;
	private String lastname;
	private String email;
	private String nombre;	
	private String marca;	
	private String categoria;
	private String consulta;
	public Asesoria(String name, String lastname, String email, String nombre, String marca, String categoria,
			String consulta) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.email = email;
		this.nombre = nombre;
		this.marca = marca;
		this.categoria = categoria;
		this.consulta = consulta;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getConsulta() {
		return consulta;
	}
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}
	
	

}
