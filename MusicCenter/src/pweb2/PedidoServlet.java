package pweb2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class PedidoServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			String name = req.getParameter("nombre");
			String apellido = req.getParameter("apellido");
			String direccion = req.getParameter("direccion");
			String ciudad = req.getParameter("ciudad");
			String correo = req.getParameter("correo");
			String nombreinst = req.getParameter("nombreinst");
			String modelo= req.getParameter("modelo");
			String marca = req.getParameter("marca");
			String categoria=req.getParameter("categoria");
			String descripcion = req.getParameter("descripcion");
			
			Pedido nuevo = new Pedido(name,apellido,direccion,ciudad,correo,nombreinst,modelo,marca,categoria,descripcion);
			BD.listapedi.add(nuevo);
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Pedido Enviado Correctamente');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("IndexUsuario.jsp");
		    rs.forward(req, resp);
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Enviar Pedido, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("formPedido.html");
	        rs.forward(req, resp);
		}
		
	}
}
