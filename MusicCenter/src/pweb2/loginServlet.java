package pweb2;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class loginServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			boolean existe=false;
			ArrayList<Usuario1> lista=BD.mylist;
			for(int i=0;i<lista.size();i++){
				Usuario1 u= lista.get(i);
				if(u.getTipoUsuario().equals("administrador")){
					existe=true;
					break;
				}
			}
			
			if(existe==false){
				Usuario1 userMaster = new Usuario1("Luis Daniel","Villanueva Montoya","danvm1.6@gmail.com","Arequipa"
						,"Calle Francisco Valencia 103 Sachaca","administrador","admin","123");
				BD.mylist.add(userMaster);
			}
			
			String id = req.getParameter("id");
			String contrasena = req.getParameter("contrasena");
			
			ArrayList<Usuario1> usuarios= BD.mylist;
			boolean validate=false;
			String tipo = null;
			for(int i=0;i<usuarios.size();i++){
				Usuario1 us = usuarios.get(i);
				if ( (us.getIdUsuario().equals(id)) && (us.getPassword().equals(contrasena)) ){
					validate=true;
					tipo=us.getTipoUsuario();
					break;
				}
			}
			
			
			if(validate==true){
				if(tipo.equals("administrador")){
					RequestDispatcher rs = req.getRequestDispatcher("IndexAdmin.jsp");
			        rs.forward(req, resp);
				}
				else if(tipo.equals("cliente")){
					RequestDispatcher rs = req.getRequestDispatcher("IndexUsuario.jsp");
			        rs.forward(req, resp);
				}
			}
			else if(validate==false){
				PrintWriter out = resp.getWriter();  
				resp.setContentType("text/html");  
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('Usuario no registrado, click en REGISTRARSE AHORA.');");  
				out.println("</script>");
				RequestDispatcher rs = req.getRequestDispatcher("index.html");
		        rs.forward(req, resp);
			}
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Error en la operacion.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("index.html");
	        rs.forward(req, resp);
		}
	}
}
