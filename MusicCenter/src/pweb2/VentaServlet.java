package pweb2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class VentaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		try{
			String numero = req.getParameter("numeroboleta");
			String fecha = req.getParameter("fechaventa");
			String nombre = req.getParameter("name");
			String categoria = req.getParameter("categoria");
			String nombreinst = req.getParameter("nombreinst");
			String cantidad = req.getParameter("cantidad");
			
			int cantidad1=Integer.parseInt(cantidad);
			
			Venta nuevo = new Venta(numero,fecha,nombre,categoria,nombreinst,cantidad1);
			BD.listaventa.add(nuevo);
			
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Venta Registrada Correctamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("IndexAdmin.jsp");
		    rs.forward(req, resp);
			
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Registrar Venta, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("formVentas.html");
	        rs.forward(req, resp);
		}
	}
}
