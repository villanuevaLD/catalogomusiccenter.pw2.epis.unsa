package pweb2;

public class Venta {
	private String numero;
	private String fecha;
	private String nombres;
	private String categoria;
	private String nombreinst;
	private int cantidad;
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getNombreinst() {
		return nombreinst;
	}
	public void setNombreinst(String nombreinst) {
		this.nombreinst = nombreinst;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Venta(String numero, String fecha, String nombres, String categoria, String nombreinst, int cantidad) {
		super();
		this.numero = numero;
		this.fecha = fecha;
		this.nombres = nombres;
		this.categoria = categoria;
		this.nombreinst = nombreinst;
		this.cantidad = cantidad;
	}
	
	
}
