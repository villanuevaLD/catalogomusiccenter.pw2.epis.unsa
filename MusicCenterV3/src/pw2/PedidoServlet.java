package pw2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import pw2.PMF;

@SuppressWarnings("serial")
public class PedidoServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String name=(String) req.getSession().getAttribute("nombreUser");
		String apellido=(String) req.getSession().getAttribute("apellidoUser");
		String correo=(String) req.getSession().getAttribute("emailUser");
		String direccion=(String) req.getSession().getAttribute("direccionUser");
		String ciudad=(String) req.getSession().getAttribute("ciudadUser");
		String nombreinst = req.getParameter("nombreinst");
		String modelo= req.getParameter("modelo");
		String marca = req.getParameter("marca");
		String categoria=req.getParameter("categoria");
		String descripcion = req.getParameter("descripcion");
		
		Pedido nuevo = new Pedido(name,apellido,direccion,ciudad,correo,nombreinst,modelo,marca,categoria,descripcion);
		//BD.listapedi.add(nuevo);
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try{
			pm.makePersistent(nuevo);
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Pedido Enviado Correctamente');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("indexUsuario.jsp");
		    rs.forward(req, resp);
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Enviar Pedido, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Pedidos.jsp");
	        rs.forward(req, resp);
		}finally{
			pm.close();
		}
		
	}
}
