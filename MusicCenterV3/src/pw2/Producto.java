package pw2;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Producto {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idProducto;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String marca;
	
	@Persistent
	private String modelo;
	
	@Persistent
	private String categoria;
	
	@Persistent
	private double precioneto;
	
	@Persistent
	private double preciobruto;
	
	@Persistent
	private int stock;
	
	@Persistent
	private double preciodscto;
	
	@Persistent
	private double longitud;
	
	@Persistent
	private double ancho;
	
	@Persistent
	private double altura;
	
	@Persistent
	private double peso;
	
	@Persistent
	private String descripcion;
	
	public Producto(String nombre, String marca, String modelo, String categoria, double precioneto, double preciobruto,
			int stock, double preciodscto, double longitud, double ancho,double altura, double peso, String descripcion) {
		super();
		this.nombre = nombre;
		this.marca = marca;
		this.modelo = modelo;
		this.categoria = categoria;
		this.precioneto = precioneto;
		this.preciobruto = preciobruto;
		this.stock = stock;
		this.preciodscto = preciodscto;
		this.longitud = longitud;
		this.ancho = ancho;
		this.altura = altura;
		this.peso = peso;
		this.descripcion = descripcion;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public double getPrecioneto() {
		return precioneto;
	}
	public void setPrecioneto(double precioneto) {
		this.precioneto = precioneto;
	}
	public double getPreciobruto() {
		return preciobruto;
	}
	public void setPreciobruto(double preciobruto) {
		this.preciobruto = preciobruto;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public double getPreciodscto() {
		return preciodscto;
	}
	public void setPreciodscto(double preciodscto) {
		this.preciodscto = preciodscto;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public double getAncho() {
		return ancho;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getIdProducto() {
		return KeyFactory.keyToString(idProducto);
	}

	public void setIdProducto(String idProducto) {
		Key keyProducto = KeyFactory.stringToKey(idProducto);
		this.idProducto = KeyFactory.createKey(keyProducto,
		Usuario1.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
}

