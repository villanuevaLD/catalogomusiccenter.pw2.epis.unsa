package pw2;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Pedido {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idPedido;
	
	@Persistent
	private String name;
	
	@Persistent
	private String lastname;
	
	@Persistent
	private String direccion;
	
	@Persistent
	private String ciudad;
	
	@Persistent
	private String email;
	
	@Persistent
	private String instrumento;
	
	@Persistent
	private String modelo;
	
	@Persistent
	private String marca;
	
	@Persistent
	private String categoria;
	
	@Persistent
	private String observaciones;
	
	public Pedido(String name, String lastname, String direccion, String ciudad, String email, String instrumento,
			String modelo, String marca, String categoria, String observaciones) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.email = email;
		this.instrumento = instrumento;
		this.modelo = modelo;
		this.marca = marca;
		this.categoria = categoria;
		this.observaciones = observaciones;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInstrumento() {
		return instrumento;
	}
	public void setInstrumento(String instrumento) {
		this.instrumento = instrumento;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getIdPedido() {
		return KeyFactory.keyToString(idPedido);
	}

	public void setIdPedido(String idPedido) {
		Key keyPedido = KeyFactory.stringToKey(idPedido);
		this.idPedido = KeyFactory.createKey(keyPedido,
		Usuario1.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}

}
