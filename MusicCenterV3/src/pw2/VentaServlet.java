package pw2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;



@SuppressWarnings("serial")
public class VentaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String numero = req.getParameter("numeroboleta");
		String fecha = req.getParameter("fechaventa");
		String nombre = req.getParameter("name");
		String categoria = req.getParameter("categoria");
		String nombreinst = req.getParameter("nombreinst");
		String cantidad = req.getParameter("cantidad");
		
		int cantidad1=Integer.parseInt(cantidad);
		
		Venta nuevo = new Venta(numero,fecha,nombre,categoria,nombreinst,cantidad1);
		//BD.listaventa.add(nuevo);
		PersistenceManager pm = PMF.get().getPersistenceManager();
		//Actualiza stock
		Query q = pm.newQuery(Producto.class);
		q.setFilter("nombre == nombreParam");
		q.declareParameters("String nombreParam");
		//Fin
		
		try{
			List<Producto> results = (List<Producto>) q.execute(nombreinst);
			if (!results.isEmpty()) {// es por esto tal vez 
			    for (Producto producto : results) {
			    	int nuevoStock=producto.getStock()-cantidad1;
			    	if(nuevoStock < 0){
			    		PrintWriter out = resp.getWriter();  
						resp.setContentType("text/html");  
						out.println("<script type=\"text/javascript\">");  
						out.println("alert('No se pudo Registrar Venta, no se cuenta con Stock');");  
						out.println("</script>");
						RequestDispatcher rs = req.getRequestDispatcher("Ventas.jsp");
				        rs.forward(req, resp);
			    	}
			    	else{
			    		producto.setStock(nuevoStock);
			    		pm.makePersistent(nuevo);
						PrintWriter out = resp.getWriter();  
						resp.setContentType("text/html");  
						out.println("<script type=\"text/javascript\">");  
						out.println("alert('Venta Registrada Correctamente');");  
						out.println("</script>");
						RequestDispatcher rs = req.getRequestDispatcher("indexAdmin.jsp");
					    rs.forward(req, resp);
			    	}
			    }
			}
			else{
				PrintWriter out = resp.getWriter();  
				resp.setContentType("text/html");  
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('No se puede registrar la venta, el producto no se encuentra registrado.');");  
				out.println("</script>");
				RequestDispatcher rs = req.getRequestDispatcher("Ventas.jsp");
			    rs.forward(req, resp);
			}
				
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Registrar Venta, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Ventas.jsp");
	        rs.forward(req, resp);
		}
		finally{
			pm.close();
		}
	}
}

