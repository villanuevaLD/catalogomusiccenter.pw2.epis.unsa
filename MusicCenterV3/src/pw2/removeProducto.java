package pw2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class removeProducto extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		String nombre =   req.getParameter("name");
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Query q = pm.newQuery(Producto.class);
		q.setFilter("nombre == nombreParam");
		q.declareParameters("String nombreParam");
		List<Producto> productos = (List<Producto>) q.execute(nombre);
		
		Producto p = null;
		if (!productos.isEmpty()) {
		    for (Producto producto : productos) {
		    	p= producto;
		    }
		}
		try{
			pm.deletePersistent(p);
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Se elimino correctamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Informes.jsp");
	        rs.forward(req, resp);
			

		}catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Eliminar el Producto');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Informes.jsp");
	        rs.forward(req, resp);
		}finally{
			pm.close();
			
		}
		
	}
}

