package pw2;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import javax.jdo.Query;


@SuppressWarnings("serial")
public class loginServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
			
		    HttpSession sesion = req.getSession();
			String id = req.getParameter("id");
			String contrasena = req.getParameter("contrasena");
			
			PersistenceManager pmf = PMF.get().getPersistenceManager();
			Query q = pmf.newQuery(Usuario1.class);
			
			q.setFilter("idUsuario == idUsuarioParam && password == passwordParam");
			q.declareParameters("String idUsuarioParam,String passwordParam");

		try{
			List<Usuario1> results = (List<Usuario1>) q.execute(id,contrasena);
			  if (!results.isEmpty()) {
			    for (Usuario1 user : results) {
			    	if( user.getTipoUsuario().equals("administrador")){
			    		sesion.setAttribute("nombreUser", user.getName());
			    		sesion.setAttribute("apellidoUser", user.getLastname());
			    		sesion.setAttribute("emailUser", user.getEmail());
			    		sesion.setAttribute("ciudadUser", user.getCiudad());
			    		sesion.setAttribute("direccionUser", user.getDireccion());
						RequestDispatcher rs = req.getRequestDispatcher("indexAdmin.jsp");
				        rs.forward(req, resp);
					}
					else if(user.getTipoUsuario().equals("cliente")){
						sesion.setAttribute("nombreUser", user.getName());
			    		sesion.setAttribute("apellidoUser", user.getLastname());
			    		sesion.setAttribute("emailUser", user.getEmail());
			    		sesion.setAttribute("ciudadUser", user.getCiudad());
			    		sesion.setAttribute("direccionUser", user.getDireccion());
						RequestDispatcher rs = req.getRequestDispatcher("indexUsuario.jsp");
				        rs.forward(req, resp);
					}
			    }
			  } else {
				  PrintWriter out = resp.getWriter();  
					resp.setContentType("text/html");  
					out.println("<script type=\"text/javascript\">");  
					out.println("alert('Usuario no registrado, click en REGISTRARSE AHORA.');");  
					out.println("</script>");
					RequestDispatcher rs = req.getRequestDispatcher("index.html");
			        rs.forward(req, resp);
			  }
		}
		    
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Error en la operacion.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("index.html");
	        rs.forward(req, resp);
		}
	}
}
