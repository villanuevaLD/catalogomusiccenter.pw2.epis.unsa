package pw2;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Venta {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idVenta;
	
	@Persistent
	private String numero;
	
	@Persistent
	private String fecha;
	
	@Persistent
	private String nombres;
	
	@Persistent
	private String categoria;
	
	@Persistent
	private String nombreinst;
	
	@Persistent
	private int cantidad;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getNombreinst() {
		return nombreinst;
	}
	public void setNombreinst(String nombreinst) {
		this.nombreinst = nombreinst;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Venta(String numero, String fecha, String nombres, String categoria, String nombreinst, int cantidad) {
		super();
		this.numero = numero;
		this.fecha = fecha;
		this.nombres = nombres;
		this.categoria = categoria;
		this.nombreinst = nombreinst;
		this.cantidad = cantidad;
	}
	
	public String getIdVenta() {
		return KeyFactory.keyToString(idVenta);
	}

	public void setIdVenta(String idVenta) {
		Key keyVenta = KeyFactory.stringToKey(idVenta);
		this.idVenta = KeyFactory.createKey(keyVenta,
		Usuario1.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
}
