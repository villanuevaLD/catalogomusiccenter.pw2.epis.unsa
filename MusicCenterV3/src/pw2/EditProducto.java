package pw2;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class EditProducto extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		String nombre = req.getParameter("nombreinst");
		String marca = req.getParameter("marca");
		String modelo = req.getParameter("modelo");
		String categoria = req.getParameter("categoria");
		String precioneto = req.getParameter("precioNeto");
		String preciobruto = req.getParameter("precioBruto");
		String stock= req.getParameter("stock");
		String preciodscto = req.getParameter("preciodescuento");
		String longitud=req.getParameter("longitud");
		String ancho=req.getParameter("ancho");
		String altura=req.getParameter("altura");
		String peso=req.getParameter("peso");
		String descripcion=req.getParameter("descripcion");
		
		double precioneto1=Double.parseDouble(precioneto);
		double preciobruto1=Double.parseDouble(preciobruto);
		int stock1=Integer.parseInt(stock);
		double preciodscto1=Double.parseDouble(preciodscto);
		double longitud1=Double.parseDouble(longitud);
		double ancho1=Double.parseDouble(ancho);
		double altura1=Double.parseDouble(altura);
		double peso1=Double.parseDouble(peso);

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Producto.class);
		q.setFilter("nombre == nombreParam");
		q.declareParameters("String nombreParam");
		List<Producto> productos = (List<Producto>) q.execute(nombre);
		
		Producto p = null;
		if (!productos.isEmpty()) {
		    for (Producto producto : productos) {
		    	p= producto;
		    }
		}
		
		try{
			p.setNombre(nombre);
			p.setMarca(marca);
			p.setModelo(modelo);
			p.setCategoria(categoria);
			p.setPrecioneto(precioneto1);
			p.setPreciobruto(preciobruto1);
			p.setStock(stock1);
			p.setPreciodscto(preciodscto1);
			p.setLongitud(longitud1);
			p.setAncho(ancho1);
			p.setAltura(altura1);
			p.setPeso(peso1);
			p.setDescripcion(descripcion);
			
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('El producto se edito correctamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Informes.jsp");
	        rs.forward(req, resp);

		}catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo editar el Producto, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Informes.jsp");
	        rs.forward(req, resp);
		}finally{
			pm.close();
		}
	}
}



