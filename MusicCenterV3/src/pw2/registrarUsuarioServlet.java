package pw2;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class registrarUsuarioServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

			String name = req.getParameter("nombre");
			String lastname = req.getParameter("apellido");
			String email = req.getParameter("correo");
			String ciudad = req.getParameter("ciudad");
			String direccion = req.getParameter("direccion");
			String tipo ="cliente";
			String id = req.getParameter("nickname");
			String contrasena = req.getParameter("contrasena");
			
			Usuario1 nuevo = new Usuario1(name,lastname,email,ciudad,direccion,tipo,id,contrasena);
			PersistenceManager pmf = PMF.get().getPersistenceManager();
			Query q = pmf.newQuery(Usuario1.class);
			
			q.setFilter("name == nameParam && lastname == lastnameParam");
			q.declareParameters("String nameParam,String lastnameParam");
			
	 try{
		 	List<Usuario1> results = (List<Usuario1>) q.execute(name,lastname);
		 	if (results.isEmpty()) {
		 		pmf.makePersistent(nuevo);
		 		PrintWriter out = resp.getWriter();  
				resp.setContentType("text/html");  
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('Usuario registrado exitosamente.');");  
				out.println("</script>");
				RequestDispatcher rs = req.getRequestDispatcher("index.html");
			    rs.forward(req, resp);
		 	}
		 	else{
		 		PrintWriter out = resp.getWriter();  
				resp.setContentType("text/html");  
				out.println("<script type=\"text/javascript\">");  
				out.println("alert('Este Usuario ya esta registrado.');");  
				out.println("</script>");
				RequestDispatcher rs = req.getRequestDispatcher("index.html");
			    rs.forward(req, resp);
		 	}
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se puedo registrar usuario, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Registrar.jsp");
	        rs.forward(req, resp);
		}finally{
			pmf.close();
		}
	}
}
