package pw2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import pw2.PMF;

@SuppressWarnings("serial")
public class AsesoriaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String name=(String) req.getSession().getAttribute("nombreUser");
		String apellido=(String) req.getSession().getAttribute("apellidoUser");
		String correo=(String) req.getSession().getAttribute("emailUser");
		String nombreinst = req.getParameter("nombreinst");
		
		String categoria=req.getParameter("categoria");
		String detalle = req.getParameter("detalle");
		
		Asesoria nuevo = new Asesoria(name,apellido,correo,nombreinst,categoria,detalle);
		//BD.listaasesoria.add(nuevo);
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			pm.makePersistent(nuevo);
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Consulta Enviada Correctamente');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("indexUsuario.jsp");
		    rs.forward(req, resp);
			
		}
		catch(Exception e){
			PrintWriter out = resp.getWriter();  
			resp.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('No se pudo Enviar Consulta, intente nuevamente.');");  
			out.println("</script>");
			RequestDispatcher rs = req.getRequestDispatcher("Asesorias.jsp");
	        rs.forward(req, resp);
		}finally{
			pm.close();
		}
	}
}
