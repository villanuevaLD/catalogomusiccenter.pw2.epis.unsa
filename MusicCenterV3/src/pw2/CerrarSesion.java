package pw2;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class CerrarSesion extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		HttpSession sesion=req.getSession();
		sesion.invalidate();
		PrintWriter out = resp.getWriter();  
		resp.setContentType("text/html");
		resp.sendRedirect("/index.html");
		//RequestDispatcher rs = req.getRequestDispatcher("index.html");
	    //rs.forward(req, resp);
		
	}
}