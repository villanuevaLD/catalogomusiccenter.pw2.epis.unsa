package pw2;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Asesoria {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idAsesoria;
	
	@Persistent
	private String name;
	
	@Persistent
	private String lastname;
	
	@Persistent
	private String email;
	
	@Persistent
	private String nombre;	
	
		
	
	@Persistent
	private String categoria;
	
	@Persistent
	private String consulta;
		
	public Asesoria(String name, String lastname, String email, String nombre, String categoria,
			String consulta) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.email = email;
		this.nombre = nombre;
		this.categoria = categoria;
		this.consulta = consulta;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getConsulta() {
		return consulta;
	}
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}
	
	public String getIdAsesoria() {
		return KeyFactory.keyToString(idAsesoria);
	}
	
	public void setIdAsesoria(String idAsesoria) {
		Key keyAsesoria = KeyFactory.stringToKey(idAsesoria);
		this.idAsesoria = KeyFactory.createKey(keyAsesoria,
		Usuario1.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}

}
