<%@ page language="java"%>
<%@ page import = "pw2.Usuario1"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
     <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li class="active"><a href="indexUsuario.jsp">Inicio</a></li>
                 			<li><a href="conocenos2.jsp">Con&oacute;cenos</a></li>
                            <li><a href="Asesorias.jsp">Asesor&iacute;as</a></li>
                            <li><a href="Pedidos.jsp">Pedidos</a></li>


        				 </ul>
                                         
                     </div>
                    <div class="col-sm-5 col-xs-12">
                    
                       <ul class="nav navbar-right">
                       
               <%String nombreUser=(String)session.getAttribute("nombreUser"); 
               String apellidoUser=(String)session.getAttribute("apellidoUser");
               String emailUser=(String)session.getAttribute("emailUser");
               %>
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong><%out.print(nombreUser);%></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="margin-left:20px; margin-top:15px;">
                                             <i class="fa fa-user fa-5x"></i>
                                        </p>
                                    </div>
                                    <div class="col-md-7" style="padding-left:2px; padding-right:2px">
                                        <p class="text-left"><strong><%out.print(nombreUser + " " + apellidoUser);%></strong></p>
                                        <p class="text-left small"><%out.print(emailUser);%></p>
                                        <p class="text-left">
                                            <a href="ModificarUsuario.jsp" class="btn btn-primary btn-block btn-sm" style="background-color:#336; padding:8px 10px; width:100%">Actualizar Datos</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="/cerrarSesion" class="btn btn-danger btn-block" style="width:60%; margin-left:75px;">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
                    </div>
                  
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.html"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
    
    

    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

                <div class="item active" style="background-image: url(images/slider/slide1.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">¿Quieres M&aacute;s Funcionalidades?</h1>
                                    <h2 class="animation animated-item-2">Si tienes consultas sobre nuestros productos o quieres realizar pedidos de Instrumentos que no tenemos en tienda. Aprovecha los beneficios de Registrarte en nuestro Portal.</h2>
                                    <a class="btn-slide animation animated-item-3" href="Registrar.html">REGISTRATE</a>
                                </div>
                            </div>

                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/imagen1.png" class="img-responsive">
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/slide3.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">Compra Ahora</h1>
                                    <h2 class="animation animated-item-2">Todos los pianos de cola, verticales y clavinovas, de todas las marcas tienen TRASLADO GRATIS a Domicilio si estas en la Ciudad de Arequipa. Si eres de otras Ciudades te asesoramos con la mejor forma de traslado.</h2>
                                    <a class="btn-slide animation animated-item-3" href="Teclados.jsp">TECLADOS Y PIANOS</a>
                                </div>
                            </div>

                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img" style="margin-top:-100px;">
                                    <img src="images/slider/imagen2.png" class="img-responsive">
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/slide2.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">Guitarras Ibanez</h1>
                                    <h2 class="animation animated-item-2">Los mejores modelos de la Marca Ibanez en Guitarras Eléctricas, Electroacústicas y Acústicas los encuentras en nuestra Tienda.</h2>
                                    <h2 class="animation animated-item-2">Encuentra la Guitarra Indicada para TI...</h2>
                                    <a class="btn-slide animation animated-item-3" href="Guitarras.jsp">Guitarras</a>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img" style="margin-left:40px; margin-top:-60px;">
                                    <img src="images/slider/imagen4.png" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->

    <section id="feature" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>¿Qué Ofrecemos?</h2>
                <p class="lead">Por la compra de Cualquiera de Nuestro Productos, Music Center Arequipa te ofrece diversos servicios, <br> que harán de tu experiencia de compra la mejor de todas  <br> Nuestro equipo especializado, músicos apasionados, está a su disposición para todo consejo.<br> Pida sin riesgo gracias al proceso de pago garantizado al 100%. <br><br> SI NO LO TENEMOS NOSOTROS, NO LO TIENE NADIE</p>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-user-md"></i>
                            <h2 style="font-size:20px">4 Años de Garantía</h2>
                            <h3>Disfrute de su Instrumento sin ninguna preocupación.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-money"></i>
                            <h2 style="font-size:20px">Devoluciones</h2>
                            <h3>Hasta 15 días para realizar cambios y devoluciones.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-truck"></i>
                            <h2 style="font-size:20px">Traslados</h2>
                            <h3>Traslados Especializados a Domicilio y Envíos Asesorados.</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-comments"></i>
                            <h2 style="font-size:20px">Asesorías Online</h2>
                            <h3>Consultas sobre nuestros productos las puede realizar por la Página Web.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-cogs"></i>
                            <h2 style="font-size:20px">Pedidos Especializados</h2>
                            <h3>Si no lo tenemos, podemos traerlo.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-refresh"></i>
                            <h2 style="font-size:20px">Productos Actualizados</h2>
                            <h3>Stock Actualizado a Tiempo Real</h3>
                        </div>
                    </div><!--/.col-md-4-->
                </div><!--/.services-->
            </div><!--/.row-->    
        </div><!--/.container-->
    </section><!--/#feature-->

    <section id="recent-works">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Lleva tu Música a Otro Nivel</h2>
                <p class="lead">Desde 2016, nuestra tienda de música ofrece una selección de instrumentos musicales, accesorios, home estudio y equipo para DJ a precios increíbles.</p>
              <p class="lead">Además ofrecemos a nuestros amigos y distinguida clientela, espectáculos en vivo dentro del show room; clínicas, demostraciones y clases maestras con reconocidos y virtuosos artistas nacionales e internacionales; visitas pedagógicas a colegios, institutos y universidades.</p>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it1.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">EARS FOR FEARS en Music Center</a> </h3>
                                <p>Antes de su presentación, la banda ochentera Tears for Fears se dió un tiempo para visitar Music Center.</p>
                                <a class="preview" href="images/portfolio/full/it1.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it2.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">Festival Fender</a></h3>
                                <p>Este año se organizó el festival, donde diversas bandas participaron para ganar diversos instrumentos de la marca.</p>
                                <a class="preview" href="images/portfolio/full/it2.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div> 

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it3.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">PIANO MAN 2016 </a></h3>
                                <p>Sorprendente la cantidad de músicos talentosos que hay en nuestra ciudad, ahora le tocó a los pianistas</p>
                                <a class="preview" href="images/portfolio/full/it3.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it4.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">Remate de Garage 2016</a></h3>
                                <p>Descuentos de hasta 70%. Se llevaron TODO</p>
                                <a class="preview" href="images/portfolio/full/it4.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   
                
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it5.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">Showrooms</a></h3>
                                <p>Teclados y sintetizadores YAMAHA desde S/.399.
Instrumentos de viento YAMAHA, JUPITER, PRELUDE desde S/.499</p>
                                <a class="preview" href="images/portfolio/full/it5.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it6.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">Fender Day</a></h3>
                                <p>Reventamos nuestro escenario en el Fender Day 2015. Se presentaron Grandes BANDAZAS</p>
                                <a class="preview" href="images/portfolio/full/it6.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i>Ver</a>
                            </div> 
                        </div>
                    </div>
                </div> 

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it7.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">Amplificadores de Tubo</a></h3>
                                <p>Toda la linea Fender. Los mejores precios del mercado y la garantia Music Center</p>
                                <a class="preview" href="images/portfolio/full/it7.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/it8.jpg" alt="">
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <h3><a href="#">BAJOS FENDER AMERICAN</a></h3>
                                <p>P-BASS $1399 - Jazz Bass a $ 1449 y todos lo modelos a los mejores precios.</p>
                                <a class="preview" href="images/portfolio/full/it8.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ver</a>
                            </div> 
                        </div>
                    </div>
                </div>   
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#recent-works-->

    <section id="partner">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Las Mejores Marcas</h2>
                <p class="lead">Music Center Utiliza las mejores marcas del mercado global <br> Aquí algunas de nuestras marcas más populares en ventas</p>
            </div>    

            <div class="partners">
                <ul>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="images/partners/fender.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="images/partners/ibanez.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="images/partners/gibson.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="images/partners/Yamaha.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="images/partners/marshall.png"></a></li>
                    
                </ul>
                
            </div>        
        </div><!--/.container-->
    </section><!--/#partner-->

    

	<footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="indexUsuario.jsp">Inicio</a>
                                ·
                                <a href="#">Guitarras y Bajos</a>
                                ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                                <a href="#">Percusi&oacute;n</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    
</body>
</html>