<%@ page language="java" contentType="text/html;"%>
<%@page import="javax.jdo.PersistenceManager,javax.jdo.Query,java.util.List,pw2.PMF, java.util.ArrayList, pw2.Usuario1,pw2.Producto,pw2.Pedido,pw2.Venta,pw2.Asesoria"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
   
    <link href="css/main.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
   <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    

    <script src="assets/js/chart-master/Chart.js"></script>
    
    
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li class="active"><a href="indexAdmin.jsp">Inicio</a></li>
                 			<li><a href="AgregarProd.jsp">Agregar Productos</a></li>
                            <li><a href="Informes.jsp">Informes</a></li>
                            <li><a href="Ventas.jsp">Registro Ventas</a></li>


        				 </ul>
                                         
                     </div>
                    <div class="col-sm-5 col-xs-12">
                    
                       <ul class="nav navbar-right">
                   
        		<%     
        	   String nombreUser=(String)session.getAttribute("nombreUser"); 
               String apellidoUser=(String)session.getAttribute("apellidoUser");
               String emailUser=(String)session.getAttribute("emailUser");
               %>               
                       
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong><%out.print(nombreUser);%></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="margin-left:20px; margin-top:15px;">
                                             <i class="fa fa-user fa-5x"></i>
                                        </p>
                                    </div>
                                    <div class="col-md-7" style="padding-left:2px; padding-right:2px">
                                        <p class="text-left"><strong><%out.print(nombreUser + " " + apellidoUser);%></strong></p>
                                        <p class="text-left small"><%out.print(emailUser);%></p>
                                        <p class="text-left">
                                            <a href="ModificarUsuario.jsp" class="btn btn-primary btn-block btn-sm" style="background-color:#336; padding:8px 10px; width:100%">Actualizar Datos</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="/cerrarSesion" class="btn btn-danger btn-block" style="width:60%; margin-left:75px;">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
                    </div>
                  
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="indexAdmin.jsp"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->

    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

                <div class="item active" style="background-image: url(images/slider/slide1.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">¿Quieres M&aacute;s Funcionalidades?</h1>
                                    <h2 class="animation animated-item-2">Si tienes consultas sobre nuestros productos o quieres realizar pedidos de Instrumentos que no tenemos en tienda. Aprovecha los beneficios de Registrarte en nuestro Portal.</h2>
                                    <a class="btn-slide animation animated-item-3" href="Registrar.html">REGISTRATE</a>
                                </div>
                            </div>

                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/imagen1.png" class="img-responsive">
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/slide3.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">Compra Ahora</h1>
                                    <h2 class="animation animated-item-2">Todos los pianos de cola, verticales y clavinovas, de todas las marcas tienen TRASLADO GRATIS a Domicilio si estas en la Ciudad de Arequipa. Si eres de otras Ciudades te asesoramos con la mejor forma de traslado.</h2>
                                    <a class="btn-slide animation animated-item-3" href="Teclados.jsp">TECLADOS Y PIANOS</a>
                                </div>
                            </div>

                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img" style="margin-top:-100px;">
                                    <img src="images/slider/imagen2.png" class="img-responsive">
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/slide2.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1" style="margin-top:-90px;">Guitarras Ibanez</h1>
                                    <h2 class="animation animated-item-2">Los mejores modelos de la Marca Ibanez en Guitarras Eléctricas, Electroacústicas y Acústicas los encuentras en nuestra Tienda.</h2>
                                    <h2 class="animation animated-item-2">Encuentra la Guitarra Indicada para TI...</h2>
                                    <a class="btn-slide animation animated-item-3" href="Guitarras.jsp">Guitarras</a>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img" style="margin-left:40px; margin-top:-60px;">
                                    <img src="images/slider/imagen4.png" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->


	<% 
		 	PersistenceManager pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(Usuario1.class);
			List<Usuario1> usuarios = (List<Usuario1>) q.execute();
			Query q2 = pm.newQuery(Producto.class);
			List<Producto> product = (List<Producto>) q2.execute();
			Query q3 = pm.newQuery(Pedido.class);
			List<Pedido> pedidos = (List<Pedido>) q3.execute();
			Query q4 = pm.newQuery(Asesoria.class);
			List<Asesoria> asesorias = (List<Asesoria>) q4.execute();
			Query q5 = pm.newQuery(Venta.class);
			List<Venta> ventas = (List<Venta>) q5.execute();
		
			
			int tamanoProducto = product.size();
			int tamanoPedido = pedidos.size();
			int tamanoUsuario = usuarios.size();
			int tamanoVentas = ventas.size();
			int tamanoAsesoria = asesorias.size();
							
%>



    <section id="feature" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Panel de Administrador</h2>   
            </div>

   <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn donut-chart">
                      			<div class="white-header">
						  			<h5>PORCENTAJE DE INFORMACI&Oacute;N</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-database"></i> 3%</p>
									</div>
	                      		</div>
								<canvas id="serverstatus01" height="120" width="120"></canvas>
								<script>
									var doughnutData = [
											{
												value: 3,
												color:"#68dff0"
											},
											{
												value : 97,
												color : "#fdfdfd"
											}
										];
										var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);
								</script>
	                      	</div><! --/grey-panel -->
                      	</div><!-- /col-md-4-->
                      	

                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn">
                      			<div class="white-header">
						  			<h5>PRODUCTOS REGISTRADOS</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-music"></i> <% out.println(tamanoProducto);%></p>
									</div>
									<div class="col-sm-6 col-xs-6"></div>
	                      		</div>
	                      		<div class="centered">
										<img src="assets/img/producto.png" width="120">
	                      		</div>
                      		</div>
                      	</div><!-- /col-md-4 -->
                      	
						<div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<div class="white-panel pn">
								<div class="white-header">
									<h5>Usuarios Registrados</h5>
								</div>
								<p><img src="assets/img/ui-zac.jpg" class="img-circle" width="100"></p>
								<p><b>Total de Usuarios</b></p>
                                <p><b><% out.println(tamanoUsuario);%></b></p>
								
							</div>
						</div><!-- /col-md-4 -->
                      	

                    </div>
                    <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="white-panel pn">
                      			<div class="white-header">
						  			<h5>VENTAS REGISTRADAS</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p><i class="fa fa-music"></i> <% out.println(tamanoVentas);%></p>
									</div>
									<div class="col-sm-6 col-xs-6"></div>
	                      		</div>
	                      		<div class="centered" >
										<img src="assets/img/registroventas.jpg" width="180">
	                      		</div>
                      		</div>
	                      	
                      	</div><!-- /col-md-4-->
                      	

                      	<div class="col-md-4 col-sm-4 mb">
                      		<div class="grey-panel pn">
								<div class="grey-header">
									<h5>REGISTRO DE PEDIDOS</h5>
								</div>
								<div class="chart mt">
									<div class="sparkline"></div>
								</div>
								<h3 class="mt"><b><% out.println(tamanoPedido);%></b><br><br>Pedidos de Usuarios</h3>
							</div>
                      	</div><!-- /col-md-4 -->
                      	
						<div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<div class="white-panel pn">
								<div class="white-header">
									<h5>ASESORÍAS</h5>
								</div>
								<p><img src="assets/img/asesores.jpg" class="img-circle" width="100"></p>
								<p><b>Consultas Recibidas</b></p>
                                <p><b><% out.println(tamanoAsesoria);%></b></p>
								
							</div>
						</div><!-- /col-md-4 -->
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<div class="content-panel pn">
								<div id="blog-bg">
									<div class="badge badge-popular">POPULAR</div>
									<div class="blog-title">El producto más Popular</div>
								</div>
								<div class="blog-text">
									<p>Las Guitarras son los instrumentos más populares en Tienda . <a href="#">Ir a Página</a></p>
								</div>
							</div>
						</div><!-- /col-md-4-->
					</div><! -- END 3RD ROW OF PANELS -->
                   
             </div>
             </div>
                
        </div><!--/.container-->
    </section><!--/#feature-->

   

    <section id="partner">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Las Mejores Marcas</h2>
                <p class="lead">Music Center Utiliza las mejores marcas del mercado global <br> Aquí algunas de nuestras marcas más populares en ventas</p>
            </div>    

            <div class="partners">
                <ul>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="images/partners/fender.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="images/partners/ibanez.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="images/partners/gibson.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="images/partners/Yamaha.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="images/partners/marshall.png"></a></li>
                    
                </ul>
                
            </div>        
        </div><!--/.container-->
    </section><!--/#partner-->

    <footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="indexAdmin.jsp">Inicio</a>
                                ·
                                <a href="#">Guitarras y Bajos</a>
                                ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                                <a href="#">Percusi&oacute;n</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
  
  
</body>
</html>