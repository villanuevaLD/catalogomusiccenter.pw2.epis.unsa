<%@ page language="java"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li class="active"><a href="index.html">Inicio</a></li>
                 			<li><a href="Conocenos1.html">Con&oacute;cenos</a></li>
        				 </ul>                   
                     </div>
                    
                    <div class="col-sm-5 col-xs-12">
                       <div class="social">
                       <div class="col-xs-4 col-md-offset-2">
                       
                       		<button  type="button" class="btn btn-default btn-primary btn-md" data-toggle="modal" data-target="#myModal" style="margin-top:-2px;">Inicia Sesi&oacute;n</button>

                          <!-- Modal -->
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
    
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style="padding:35px 50px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4><span class="glyphicon glyphicon-lock"></span>&nbsp;Inicia Sesi&oacute;n</h4>
                                </div>
                               	<div class="modal-body" style="padding:40px 50px;">
                                  <form role="form" method="post" action="/loginServlet">
                                    <div class="form-group">
                                      <label for="usrname"><span class="glyphicon glyphicon-user"></span> Usuario</label>
                                      <br>
                                      <input type="text" class="form-control" name="id" id="nombre" required placeholder="Ingrese su Usuario">
                                    </div>
                                    <div class="form-group">
                                      <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                                      <input type="text" class="form-control" name="contrasena" id="pass" required placeholder="Ingrese Password">
                                    </div>
                                    
                                      <button type="submit" class="btn  btn-default btn-block"><span class="glyphicon glyphicon-off"></span> Ingresar</button>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Salir</button>
                                  <br>
                                  <br>
                                  <p>¿NO EST&Aacute; REGISTRADO?<a href="Registrar.jsp">&nbsp;&nbsp;REGISTRARSE AQU&Iacute;</a></p>
                                  
                                </div>
                              </div>
                              
                            </div>
                          </div>
                        </div>
 
						<script>
                        $(document).ready(function(){
                            $("#myBtn").click(function(){
                                $("#myModal").modal();
                            });
                        });
                        </script>
                       		 
                       </div>
                         
                      </div>
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.html"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header--><!--/#main-slider-->

    <div class="container">

	<div class="page-content-wrap" style="margin-top:20px">
                
                    <div class="row">
                        <div class="col-md-12">
                            <!--Aqui empieza fomulario de asesorias-->
                            <form class="form-horizontal" method="post" action="registrarUsuarioServlet">
                            <div class="panel panel-default">
                                
                                <div class="panel-heading" style="border-bottom:solid #CCC 1px; padding-top:25px; padding-left:25px">
                                    <h3 class="panel-title"><strong style="font-size:35px">Registrar Nuevo Usuario</strong> Formulario</h3>   
                                </div>
                                
                                <div class="panel-body" style="padding-top:50px">                                                                                                          								 <div class="row">
                                        
                                        <div class="col-md-6">
                                        <fieldset>
                                        <legend> Datos del Usuario</legend>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nombre de Usuario </label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                                        <input type="text" class="form-control" name="nickname"/>
                                                    </div>                                            
                                                    <span class="help-block">Ingrese el Nombre con el que Ingresará al Sistema</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Password</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-eye"></span></span>
                                                        
                                                        <input type="text" class="form-control" name="contrasena" id="pass" required>
                                                    </div>            
                                                    <span class="help-block">Ingrese su password</span>
                                                </div>
                                            </div>                                                                                     
                                            </fieldset>                                                                                                                            
                                        </div>
                               
                                <div class="col-md-6">
                                <fieldset>
                                <legend>Datos del Cliente</legend>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Nombres</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                                        <input type="text" class="form-control" name="nombre" required>                                            
                                                    </div>
                                                    <span class="help-block">Ingrese sus nombres</span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Apellidos</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                                        <input type="text" class="form-control" name="apellido" required>                                            
                                                    </div>
                                                    <span class="help-block">Ingrese sus Apellidos</span>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Correo Electrónico</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-at"></span></span>
                                                        <input type="email" class="form-control" name="correo" required>                                            
                                                    </div>
                                                    <span class="help-block">Ingrese su e-mail</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Dirección</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-map-marker" aria-hidden="true"></span></span>
                                                        <input type="text" class="form-control" name="direccion">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese su Dirección</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ciudad</label>
                                                <div class="col-md-9">                                                                                            
                                                    <select class="form-control select"  name="ciudad">
                                                        <option value=""></option>
														<option value="Amazonas">Amazonas</option>
														<option value="Ancash">Ancash</option>
														<option value="Apurimac">Apurimac</option>
														<option value="Arequipa">Arequipa</option>
														<option value="Ayacucho">Ayacucho</option>
														<option value="Cajamarca">Cajamarca</option>
														<option value="Cuzco">Cuzco</option>
														<option value="Huancavelica">Huancavelica</option>
														<option value="Huanuco">Huanuco</option>
														<option value="Ica">Ica</option>
														<option value="Junin">Junin</option>
														<option value="LaLibertad">La Libertad</option>
														<option value="Lambayeque">Lambayeque</option>
														<option value="Lima">Lima</option>
														<option value="Loreto">Loreto</option>
														<option value="MadredeDios">Madre de Dios</option>
														<option value="Moquegua">Moquegua</option>
														<option value="Pasco">Pasco</option>
														<option value="Piura">Piura</option>
														<option value="Puno">Puno</option>
														<option value="SanMartin">San Martin</option>
														<option value="Tacna">Tacna</option>
														<option value="Tumbes">Tumbes</option>
														<option value="Ucayali">Ucayali</option>
                                                    </select>
                                                    <span class="help-block">Seleccione su Ciudad</span>
                                                </div>
                                            </div>
                         
                                        </fieldset>
                                  </div>
                                      
                                </div>
                            </div>
                                        
                                        
                                        
                                        
                                <div class="panel-footer">
                                    <button class="btn btn-default">Limpiar</button>                                    
                                    <button class="btn btn-primary pull-right" style="background-color:#390; margin-top:-1px">Registrar</button>
                                </div>
                                
                            </div>
                            </form>
                            </div> <!--Panel-default-->
                            
                            
                            
                        </div>
                    </div>                    
                    
                </div>

    <footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="index.html">Inicio</a>
                                ·
                                <a href="#">Guitarras</a>
                                ·
                                <a href="#">Bajos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                               
                                <a href="#">Percusi&oacute;n</a>
                                 ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    
</body>
</html>