<%@ page language="java"%>
<%@page import="javax.jdo.PersistenceManager,javax.jdo.Query,java.util.List,pw2.PMF, java.util.ArrayList, pw2.Usuario1,pw2.Producto,pw2.Pedido,pw2.Venta,pw2.Asesoria"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li><a href="indexUsuario.jsp">Inicio</a></li>
                 			<li class="active"><a href="conocenos2.jsp">Conócenos</a></li>
                            <li><a href="Asesorias.jsp">Asesorías</a></li>
                            <li><a href="Pedidos.jsp">Pedidos</a></li>
        				 </ul>                   
                     </div>
                    
                    <div class="col-sm-5 col-xs-12">
                     <%     
		        	   String nombreUser=(String)session.getAttribute("nombreUser"); 
		               String apellidoUser=(String)session.getAttribute("apellidoUser");
		               String emailUser=(String)session.getAttribute("emailUser");
               		%> 
                    
                       <ul class="nav navbar-right">
                		<li class="dropdown">
                    		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        	<span class="glyphicon glyphicon-user"></span> 
                        	<strong><%out.print(nombreUser);%></strong>
                        	<span class="glyphicon glyphicon-chevron-down"></span>
                    		</a>
                    		<ul class="dropdown-menu" >
                        	<li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="margin-left:20px; margin-top:15px;">
                                             <i class="fa fa-user fa-5x"></i>
                                        </p>
                                    </div>
                                   <div class="col-md-5" style="padding-left:30px; padding-right:25px">
                                        <p class="text-left"><strong><%out.print(nombreUser + " " + apellidoUser);%></strong></p>
                                        <p class="text-left small"><%out.print(emailUser);%></p>
                                        <p class="text-left">
                                            <a href="ModificarUsuario.jsp" class="btn btn-primary btn-block btn-sm" style="background-color:#336; padding:8px 10px; width:100%">Actualizar Datos</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="/cerrarSesion" class="btn btn-danger btn-block" style="width:60%; margin-left:75px;">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
                    </div>
                    
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.html"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->

   <section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown" >
				<h2>Acerca de Nosotros</h2>
				<p class="lead">MUSIC CENTER nace a mediados del mes de abril del año 2008, con la misión de suplir las necesidades de los músicos y profesionales <br>  del sonido de nuestro país. Somos una empresa 100% peruana, dedicada al asesoramiento y venta de instrumentos musicales.</p>
              <p class="lead">Nuestro staff está conformado por músicos profesionales e ingenieros de sonido, todos ellos de reconocida trayectoria en el medio artístico, <br> quienes garantizan un correcto análisis para la adquisición del instrumento musical, adecuado a la necesidad de cada uno  de nuestros clientes.</p>
              <p class="lead">En todo esto se basa el prestigio de MUSIC CENTER, siempre preocupados por la calidad y excelencia en la atención y satisfacción en la entrega <br> de las mejores marcas del mundo, todas ellas necesarias para la profesionalización de la música en nuestro país. </p>

		  </div>
			
			<!-- about us slider -->
			<div id="about-slider">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
				  	<ol class="carousel-indicators visible-xs">
					    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-slider" data-slide-to="1"></li>
					    <li data-target="#carousel-slider" data-slide-to="2"></li>
				  	</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img src="images/banneraboutus.jpg" class="img-responsive" alt=""> 
					   </div>
					   <div class="item">
							<img src='images/Music+Webstore+Banner.jpg' class="img-responsive" alt=""> 
					   </div> 
					   <div class="item">
							<img src="images/home-slide2.jpg" class="img-responsive" alt=""> 
					   </div> 
					</div>
					
					<a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
						<i class="fa fa-angle-left"></i> 
					</a>
					
					<a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
						<i class="fa fa-angle-right"></i> 
					</a>
				</div> <!--/#carousel-slider-->
			</div><!--/#about-slider-->
			
			
			<!-- Our Skill -->
			<div class="skill-wrap clearfix">
			
				<div class="center wow fadeInDown">
                <br>
					<h2>Misión</h2>
					<p class="lead">MUSIC CENTER tiene como misión trabajar por el desarrollo musical y cultural del Perú, ofreciendo a nuestros clientes una gran variedad de instrumentos musicales de alta calidad, con información concreta y especializada un mercado precario y desabastecido de instrumentos y equipos de gran calidad; Además de impulsar la innovación tecnológica y el desarrollo de nuestros servicios, asegurando el permanente progreso de la empresa y mayor satisfacción de nuestros clientes.</p>
                    <br>
                    <hr>
                    <br>
                    <h2>Visión</h2>
					<p class="lead">Ser una empresa líder en la rama de audio profesional, instrumentos y accesorios musicales. mantener una constante evolución a través de la mejora continua en nuestros métodos de trabajo en equipo, fomentando siempre los mejores valores humanos y corporativos.</p>
				</div>
                
             </div>
           </div>
           
           </section>

   


    <footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="indexUsuario.jsp">Inicio</a>
                                ·
                                <a href="#">Guitarras y Bajos</a>
                                ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                                <a href="#">Percusi&oacute;n</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    
</body>
</html>