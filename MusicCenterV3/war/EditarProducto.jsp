<%@ page language="java"%>
<%@page import="javax.jdo.PersistenceManager,javax.jdo.Query,java.util.List,pw2.PMF, java.util.ArrayList, pw2.Usuario1,pw2.Producto,pw2.Pedido,pw2.Venta,pw2.Asesoria"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
   <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
  
    
    
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li><a href="indexAdmin.jsp">Inicio</a></li>
                 			<li><a href="AgregarProd.jsp">Agregar Productos</a></li>
                            <li><a href="Informes.jsp">Informes</a></li>
                            <li><a href="Ventas.jsp">Registro Ventas</a></li>


        				 </ul>
                                         
                     </div>
                    <div class="col-sm-5 col-xs-12">
                    
                       <ul class="nav navbar-right">
                       
                <%     
        	   String nombreUser=(String)session.getAttribute("nombreUser"); 
               String apellidoUser=(String)session.getAttribute("apellidoUser");
               String emailUser=(String)session.getAttribute("emailUser");
               %>  
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong><%out.print(nombreUser);%></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="margin-left:20px; margin-top:15px;">
                                             <i class="fa fa-user fa-5x"></i>
                                        </p>
                                    </div>
                                    <div class="col-md-5" style="padding-left:30px; padding-right:25px">
                                        <p class="text-left"><strong><%out.print(nombreUser + " " + apellidoUser);%></strong></p>
                                        <p class="text-left small"><%out.print(emailUser);%></p>
                                        <p class="text-left">
                                            <a href="ModificarUsuario.jsp" class="btn btn-primary btn-block btn-sm" style="background-color:#336; padding:8px 10px; width:100%">Actualizar Datos</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="/cerrarSesion" class="btn btn-danger btn-block" style="width:60%; margin-left:75px;">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
                    </div>
                  
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="indexAdmin.jsp"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
    
    <% 
		String name= request.getParameter("name");
    	PersistenceManager pm = PMF.get().getPersistenceManager();
	
		Query q = pm.newQuery(Producto.class);
		q.setFilter("nombre == nombreParam");
		q.declareParameters("String nombreParam");
		List<Producto> productos = (List<Producto>) q.execute(name);
	
		Producto p = null;
		if (!productos.isEmpty()) {
	  	  for (Producto producto : productos) {
	    		p= producto;
	    	}
		}
	%>
    
    
<div class="container">

	<div class="page-content-wrap" style="margin-top:20px">
                
                    <div class="row">
                        <div class="col-md-12">
                            <!--Aqui empieza fomulario de asesorias-->
                            <form class="form-horizontal" method="post" action="editProducto" >
                            <div class="panel panel-default">
                                
                                <div class="panel-heading" style="border-bottom:solid #CCC 1px; padding-top:25px; padding-left:25px">
                                    <h3 class="panel-title"><strong style="font-size:35px">Editar Producto</strong> Formulario</h3>   
                                </div>
                                
                                <div class="panel-body">                                                                                                          								 <div class="row">
                                        
                                        <div class="col-md-6">
                                        <fieldset>
                                        <legend> Datos del Producto </legend>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nombre del Instrumento</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="nombreinst" value="<%out.println(p.getNombre());%>" required/>
                                                    </div>                                            
                                                    <span class="help-block">Ingrese el Nombre del Instrumento a Agregar</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Modelo</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="modelo" value="<%out.print(p.getModelo());%>"/>
                                                    </div>            
                                                    <span class="help-block">Ingrese el modelo o número de Serie del Instrumento</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Marca</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control"  name="marca" value="<%out.print(p.getMarca());%>"/>
                                                    </div>            
                                                    <span class="help-block">Ingrese la marca del Instrumento</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Categoría</label>
                                                <div class="col-md-9">                                                                                            
                                                    <select class="form-control select"  name="categoria">
                                                        <option value="Guitarras">Guitarras</option>
                                                        <option value="Bajos">Bajos</option>
                                                        <option value="Teclados">Teclados</option>
                                                        <option value="Percusion">Percusión</option>
                                                        <option value="Vientos">Vientos</option>
                                                        <option value="Amplificadores">Amplificadores</option>
                                                    </select>
                                                    <span class="help-block">Seleccione la categoría del Instrumento</span>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Imagen 1</label>
                                                <div class="col-md-9">                                                                                                                                        
                                                    <input type="file" class="fileinput btn-primary" name="imagen1" id="filename" title="Browse file" style="background-color:#903"/>
                                                    <span class="help-block">Cargue la Imagen 1 a mostrar del Producto</span>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Imagen 2</label>
                                                <div class="col-md-9">                                                                                                                                        
                                                    <input type="file" class="fileinput btn-primary" name="imagen2" id="filename" title="Browse file" style="background-color:#903"/>
                                                    <span class="help-block">Cargue la Imagen 2 a mostrar del Producto</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Descripción del Producto</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <textarea class="form-control" rows="5" name="descripcion" ><%out.println(p.getDescripcion());%></textarea>
                                                    <span class="help-block">Ingrese la descripción detallada del Producto</span>
                                                </div>
                                            </div>
                                            </fieldset>            
                                        </div>

                                <div class="col-md-6">
                                <fieldset>
                                <legend>Especificaciones</legend>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Longitud (cm)</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-arrows-h"></span></span>
                                                        <input type="text" class="form-control" name="longitud" maxlength="5" value="<%out.print(p.getLongitud());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese la Longitud del Producto</span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Altura (cm)</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-arrows-v"></span></span>
                                                        <input type="text" class="form-control" name="altura" maxlength="5" value="<%out.print(p.getAltura());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese la Altura del Producto</span>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Ancho(cm)</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-expand"></span></span>
                                                        <input type="text" class="form-control" name="ancho" maxlength="5" value="<%out.print(p.getAncho());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese el Ancho del Producto</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Peso(kg)</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-balance-scale" aria-hidden="true"></span></span>
                                                        <input type="text" class="form-control" name="peso" maxlength="5" value="<%out.print(p.getPeso());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese el Peso</span>
                                                </div>
                                            </div>
                                                        
                                       
                                        </fieldset>
                                  </div>
                                  
                                  <div class="col-md-6">
                                <fieldset>
                                <legend>Precio y Stock </legend>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Precio Producto (Neto): S/.</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa  fa-usd"></span></span>
                                                        <input type="text" class="form-control" name="precioNeto" maxlength="5" pattern="+[1-9]*[0-9]{3}\\+.*[0.9]{2}" value="<%out.print(p.getPrecioneto());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese precio Neto sin Impuestos ni Descuentos</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Precio Producto (Bruto): S/.</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa  fa-usd"></span></span>
                                                        <input type="text" name="precioBruto" class="form-control"  maxlength="5" pattern="+[1-9]*[0-9]{3}\\+.*[0.9]{2}" value="<%out.print(p.getPreciobruto());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese precio Bruto o precion de Venta junto con Impuestos</span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Precio Descuento: S/.</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa  fa-usd"></span></span>
                                                        <input type="text" name="preciodescuento" class="form-control"  maxlength="5" pattern="+[1-9]*[0-9]{3}\\+.*[0.9]{2}" value="<%out.print(p.getPreciodscto());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese precio para Decuentos</span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Stock</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-refresh"></span></span>
                                                        <input type="text" class="form-control" name="stock" maxlength="5" pattern="[0-9]" value="<%out.print(p.getStock());%>">                                            
                                                    </div>
                                                    <span class="help-block">Ingrese Stock actual del Producto</span>
                                                </div>
                                            </div>
                                       
                                        </fieldset>
                                  </div>
                                        
                                        </div>
                                        </div>
         
                                <div class="panel-footer">
                                    <button class="btn btn-default">Limpiar</button>                                    
                                    <button class="btn btn-primary pull-right" style="background-color:#390; margin-top:-1px">Modificar</button>
                                </div>
                                
                            </div>
                            
                            </div> <!--Panel-default-->
                            
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                </div>




    <footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="#">Inicio</a>
                                ·
                                <a href="#">Guitarras y Bajos</a>
                                ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                                <a href="#">Percusi&oacute;n</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/jquery.smartWizard-2.0.min.js"></script>   
  
  
</body>
</html>