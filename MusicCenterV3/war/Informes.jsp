<%@ page language="java"%>
<%@page import="javax.jdo.PersistenceManager,javax.jdo.Query,java.util.List,pw2.PMF, java.util.ArrayList, pw2.Usuario1,pw2.Producto,pw2.Pedido,pw2.Venta,pw2.Asesoria"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MusicCenter</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    <link href=" https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
   <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
        
</head><!--/head-->

<body class="homepage">
<!-- Listas de Queries para informes -->
	<% 
		 	PersistenceManager pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(Usuario1.class);
			List<Usuario1> usuarios = (List<Usuario1>) q.execute();
			Query q2 = pm.newQuery(Producto.class);
			List<Producto> productos = (List<Producto>) q2.execute();
			Query q3 = pm.newQuery(Pedido.class);
			List<Pedido> pedidos = (List<Pedido>) q3.execute();
			Query q4 = pm.newQuery(Asesoria.class);
			List<Asesoria> asesorias = (List<Asesoria>) q4.execute();
			Query q5 = pm.newQuery(Venta.class);
			List<Venta> ventas = (List<Venta>) q5.execute();						
%>
    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-5 col-md-offset-1">
                         <ul class="nav nav-tabs">
                 			<li><a href="indexAdmin.jsp">Inicio</a></li>
                 			<li><a href="AgregarProd.jsp">Agregar Productos</a></li>
                            <li class="active"><a href="Informes.jsp">Informes</a></li>
                            <li><a href="Ventas.jsp">Registro Ventas</a></li>
        				 </ul>                                        
                     </div>
                     
                    <div class="col-sm-5 col-xs-12">
                       <ul class="nav navbar-right">
			                <%     
			        	   String nombreUser=(String)session.getAttribute("nombreUser"); 
			               String apellidoUser=(String)session.getAttribute("apellidoUser");
			               String emailUser=(String)session.getAttribute("emailUser");
			               %>        
      
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong><%out.print(nombreUser);%></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="margin-left:20px; margin-top:15px;">
                                             <i class="fa fa-user fa-5x"></i>
                                        </p>
                                    </div>
                                    <div class="col-md-5" style="padding-left:30px; padding-right:25px">
                                        <p class="text-left"><strong><%out.print(nombreUser + " " + apellidoUser);%></strong></p>
                                        <p class="text-left small"><%out.print(emailUser);%></p>
                                        <p class="text-left">
                                            <a href="ModificarUsuario.jsp" class="btn btn-primary btn-block btn-sm" style="background-color:#336; padding:8px 10px; width:100%">Actualizar Datos</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="/cerrarSesion" class="btn btn-danger btn-block" style="width:60%; margin-left:75px;">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
                    </div>
                  
                    </div>

                
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">NAV</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="indexAdmin.jsp"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Guitarras</a></li>
                        <li><a href="services.html">Bajos</a></li>
                        <li><a href="services.html">Teclados</a></li>
                        <li><a href="portfolio.html">Percusi&oacute;n</a></li>
                        <li><a href="portfolio.html">Vientos</a></li>
                        <li><a href="blog.html">Amplificadores</a></li> 
                                               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
    
    <div class="container">
           
    <div class="page-content-wrap">
    
                
                    <div class="row">

                        <div class="col-md-12">
                        <ul class="nav nav-tabs nav-justified" role="tablist" style="height:4px">
			                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Productos</a></li>
			                <li><a href="#tab-second" role="tab" data-toggle="tab">Usuarios</a></li>
			                <li><a href="#tab-third" role="tab" data-toggle="tab">Ventas</a></li>
			                <li><a href="#tab-four" role="tab" data-toggle="tab">Asesor&iacute;as</a></li>
			                <li><a href="#tab-five" role="tab" data-toggle="tab">Pedidos</a></li>
             			</ul>
                                                            
                                <div class="panel panel-default tabs">                            
                                    
                                <div class="panel-body tab-content">
                            		<div class="tab-pane active" id="tab-first">
                                            <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                      	  
                          <table class="table table-striped table-advance table-hover" id="exportTable">
	                  	  	  <h4 style="background-color:#036">Informe de Productos Registrados</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-user"></i> Nombre</th>
                                  <th><i class="fa fa-user"></i> Marca</th>
                                  <th><i class="fa fa-user"></i> Modelo</th>
                                  <th><i class="fa fa-at"></i>Categor&iacute;a </th>
                                  <th><i class=" fa fa-edit"></i> Precio Neto</th>
                                  <th><i class=" fa fa-edit"></i> Stock</th>
                                  <th><i class=" fa fa-edit"></i> Opciones</th>
                                
                              </tr>
                              </thead>
                              <tbody>
                              
                              <% for (Producto producto : productos){ %>
                              <tr>
                              	<td><% out.println(producto.getNombre());%></td>
                              	<td><% out.println(producto.getMarca());%></td>
                              	<td><% out.println(producto.getModelo());%></td>
                              	<td><% out.println(producto.getCategoria());%></td>
                              	<td><% out.println(producto.getPrecioneto());%></td>
                              	<td><% out.println(producto.getStock());%></td>
                              	 <td> 
                                      <button class="btn btn-success btn-xs" onclick="location.href='/EditarProducto.jsp?name=<%= producto.getNombre() %>'"><i class="fa fa-edit"></i></button>
                                      
                                      <button class="btn btn-danger btn-xs" onclick="location.href='/removeProducto?name=<%= producto.getNombre() %>'"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
                              <% } %>                                                                                     
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div>
			</div>
               <div class="tab-pane" id="tab-second">
                                        
                               <div class="row mt">
					                  <div class="col-md-12">
					                      <div class="content-panel">
					                          <table class="table table-striped table-advance table-hover">
						                  	  	  <h4 style="background-color:#036">Informe de Usuarios registrados</h4>
						                  	  	  <hr>
					                              <thead>
					                              <tr>
					                                  <th><i class="fa fa-user"></i> Nombres</th>
					                                  <th><i class="fa fa-user"></i> Apellidos</th>
					                                  <th><i class="fa fa-at"></i> E-mail</th>
					                                  <th><i class="fa fa-at"></i>Direcci&oacute;n</th>
					                                  <th><i class=" fa fa-edit"></i>Ciudad</th>
					                                  
					                              </tr>
					                              </thead>
					                              <tbody>
					                              
					                              <% for (Usuario1 user : usuarios){ %>
					                              <tr>
					                              	<td><% out.println(user.getName());%></td>
					                              	<td><% out.println(user.getLastname());%></td>
					                              	<td><% out.println(user.getEmail());%></td>
					                              	<td><% out.println(user.getDireccion());%></td>
					                              	<td><% out.println(user.getCiudad());%></td>
					                              	
					                              </tr>
					                              <% } %>                                                                                     
					                              </tbody>
					                          </table>
					                      </div><!-- /content-panel -->
					                  </div><!-- /col-md-12 -->
					              </div>              
                             </div>     
                             
                                                                
                              <div class="tab-pane" id="tab-third">
                              	<div class="row mt">
					                  <div class="col-md-12">
					                      <div class="content-panel">
					                          <table class="table table-striped table-advance table-hover">
						                  	  	  <h4 style="background-color:#036">Informe de Ventas Registradas</h4>
						                  	  	  <hr>
					                              <thead>
					                              <tr>
					                                  <th><i class="fa fa-user"></i>N&uacute;mero de Factura</th>
					                                  <th><i class="fa fa-user"></i>Fecha de Venta</th>
					                                  <th><i class="fa fa-at"></i>Cliente</th>
					                                  <th><i class="fa fa-at"></i>Producto</th>
					                                  <th><i class=" fa fa-edit"></i>Categor&iacute;a</th>
					                                  <th><i class=" fa fa-edit"></i>Cantidad</th>
					                                  
					                              </tr>
					                              </thead>
					                              <tbody>
					                              
					                              <% for (Venta venta : ventas){ %>
					                              <tr>
					                              	<td><% out.println(venta.getNumero());%></td>
					                              	<td><% out.println(venta.getFecha());%></td>
					                              	<td><% out.println(venta.getNombres());%></td>
					                              	<td><% out.println(venta.getNombreinst());%></td>
					                              	<td><% out.println(venta.getCategoria());%></td>
					                              	<td><% out.println(venta.getCantidad());%></td>
					                              	
					                              </tr>
					                              <% } %>                                                                                     
					                              </tbody>
					                          </table>
					                      </div><!-- /content-panel -->
					                  </div><!-- /col-md-12 -->
					              </div>  
            
                              </div>
                              <!-- Tabla de Asesorias -->
                              <div class="tab-pane" id="tab-four">
                              	<div class="row mt">
					                  <div class="col-md-12">
					                      <div class="content-panel">
					                          <table class="table table-striped table-advance table-hover">
						                  	  	  <h4 style="background-color:#036">Asesor&iacute;as Recibidas</h4>
						                  	  	  <hr>
					                              <thead>
					                              <tr>
					                                  <th><i class="fa fa-user"></i>Nombre de Cliente</th>
					                                  <th><i class="fa fa-edit"></i>Producto</th>
					                                  <th><i class=" fa fa-edit"></i>Categor&iacute;a</th>
					                                  <th><i class=" fa fa-edit"></i>Consulta</th>
					                                  <th><i class="fa fa-at"></i>E-mail</th>
					                              </tr>
					                              </thead>
					                              <tbody>
					                              
					                              <% for (Asesoria asesoria : asesorias){ %>
					                              <tr>
					                              	<td><% out.println(asesoria.getName()+ " "+asesoria.getLastname());%></td>
					                              	<td><% out.println(asesoria.getNombre());%></td>
					                              	<td><% out.println(asesoria.getCategoria());%></td>
					                              	<td><% out.println(asesoria.getConsulta());%></td>
					                              	<td><a href="http://www.gmail.com"><%out.println(asesoria.getEmail()); %></a></td>
					                              </tr>
					                              <% } %>                                                                                     
					                              </tbody>
					                          </table>
					                      </div><!-- /content-panel -->
					                  </div><!-- /col-md-12 -->
					              </div> 
            
                              </div> <!-- Fin informe Asesorias -->
                               
                              <!-- Informe Pedidos -->     
                              <div class="tab-pane" id="tab-five">
                              	<div class="row mt">
					                  <div class="col-md-12">
					                      <div class="content-panel">
					                          <table class="table table-striped table-advance table-hover">
						                  	  	  <h4 style="background-color:#036">Pedidos Registrados</h4>
						                  	  	  <hr>
					                              <thead>
					                              <tr>
					                                  <th><i class="fa fa-user"></i>Nombre de Cliente</th>
					                                  <th><i class="fa fa-edit"></i>Ciudad</th>
					                                  <th><i class="fa fa-edit"></i>Producto</th>
					                                  <th><i class="fa fa-edit"></i>Modelo</th>
					                                  <th><i class=" fa fa-edit"></i>Categor&iacute;a</th>
					                                  <th><i class=" fa fa-edit"></i>Descripci&oacute;n Pedido</th>
					                                  <th><i class="fa fa-at"></i>E-mail</th>
					                              </tr>
					                              </thead>
					                              <tbody>
					                              
					                              <% for (Pedido pedido : pedidos){ %>
					                              <tr>
					                              	<td><% out.println(pedido.getName()+ " "+pedido.getLastname());%></td>
					                              	<td><% out.println(pedido.getCiudad());%></td>
					                              	<td><% out.println(pedido.getInstrumento());%></td>
					                              	<td><% out.println(pedido.getModelo());%></td>
					                              	<td><% out.println(pedido.getCategoria());%></td>
					                              	<td><% out.println(pedido.getObservaciones());%></td>
					                              	<td><a href="http://www.gmail.com"><%out.println(pedido.getEmail()); %></a></td>			                             
					                              	
					                              </tr>
					                              <% } %>                                                                                     
					                              </tbody>
					                          </table>
					                      </div><!-- /content-panel -->
					                  </div><!-- /col-md-12 -->
					              </div>  
            
                              </div>
                              
                              
                              
                              <!-- Fin informe Pedidos -->
                                    
                                    
                          
                                    </div>
                                    
                                </div>                                
                            
                           
                            
                        </div>
                    </div>                    
                    
                </div>
    
    </div>

    <footer id="footer" class="midnight-blue">
       <section id="mainFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="footerWidget">
                    <br>
						<h3 style="color:  #ffffff; font: normal 60px 'Cookie', cursive; margin: 0;">Music<span style="color:#930;">Center</span></h3>
                        <br>

                            <p class="footer-links" style="display:inline-block; line-height: 1.5;">
                                <a href="indexAdmin.jsp">Inicio</a>
                                ·
                                <a href="#">Guitarras y Bajos</a>
                                ·
                                <a href="#">Vientos</a>
                                ·
                                <a href="#">Teclados</a>
                                ·
                                <a href="#">Percusi&oacute;n</a>
                                ·
                                <a href="#">Amplificadores</a>
                            </p>
                            <hr>
                            <p class="footer-company-name" style="color:  #8f9296; font-size: 14px; font-weight: normal; text-align:center">Programaci&oacute;n Web 2 &copy; 2016</p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="footerWidget">
                    <br>
						<div>
							<i class="fa fa-map-marker"></i>
							<p><span>	
							Av. Victor Andrés Belaunde 347, Yanahuara.</span> Arequipa - Per&uacute;</p>
						</div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>201-1800 • 242-6377</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p><a href="mailto:ventas@musiccenter.com.pe">ventas@musiccenter.com.pe</a></p>
                        </div>
                                
						
					</div>
				</div>
					<div class="col-sm-3">
						<div class="footerWidget">
                        
                        <br>
							<span>Horarios de Atenci&oacute;n</span>
                    Atención de lunes a sábados de 11:00am. a 8:00pm.
                    <br>
                    <br>
							<ul class="socialNetwork">
								<li><a href="#" class="tips" title="Síguenos en Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Twitter"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Google+"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tips" title="Síguenos en Youtube"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a></li>
								
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</section>
		
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
   
  
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    
    
</body>
</html>